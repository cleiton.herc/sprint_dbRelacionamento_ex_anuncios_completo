package com.itau.anuncios.repository;
import com.itau.anuncios.model.Usuario;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;


public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{
	Optional<Usuario> findByEmail(String email);

	Optional<Usuario> findById(long id);
}
