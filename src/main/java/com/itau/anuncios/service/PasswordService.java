package com.itau.anuncios.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordService {
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	
	public String encode(String senha) {
		return encoder.encode(senha);
	}
	
	public boolean verificar(String senha, String hash) {
		return encoder.matches(senha, hash);
	}
}
