package com.itau.anuncios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.anuncios.model.Anuncio;
import com.itau.anuncios.repository.AnuncioRepository;

@Controller
public class AnuncioController {
	@Autowired
	AnuncioRepository anuncioRepository;

	@RequestMapping(path="/anuncio", method=RequestMethod.POST)
	@ResponseBody
	public Anuncio cadastraAnuncio(@RequestBody Anuncio anuncio) {
		return anuncioRepository.save(anuncio);
	}
}

