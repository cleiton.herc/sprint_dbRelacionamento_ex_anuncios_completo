package com.itau.anuncios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.anuncios.model.Oferta;
import com.itau.anuncios.repository.OfertaRepository;

public class OfertaController {
	@Autowired
	OfertaRepository ofertaRepository;
	
	@RequestMapping(path="/oferta", method=RequestMethod.POST)
	@ResponseBody
	public Oferta cadastraOferta(@RequestBody Oferta oferta) {
		return ofertaRepository.save(oferta);
	}
}

